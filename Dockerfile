FROM condaforge/mambaforge:latest

SHELL ["bash", "-c"]

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -q -y --no-install-recommends \
    libgl1-mesa-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*


RUN conda config --set always_yes yes --set changeps1 no \
    && wget https://gitlab.com/protis/protis/-/raw/main/environment.yml -q \
    && sed -i 's/name: protis/name: base/g' environment.yml \
    && mamba install make cmake compilers swig \
    && mamba env update -n base -f environment.yml \
    && rm -f environment.yml \
    && pip cache purge \
    && conda clean --tarballs --index-cache --packages \
    && find ${CONDA_DIR} -follow -type f -name '*.a' -delete \
    && find ${CONDA_DIR} -follow -type f -name '*.pyc' -delete \
    && find ${CONDA_DIR} -follow -type f -name '*.js.map' -delete