

.. image:: https://img.shields.io/gitlab/pipeline/protis/docker/main?style=for-the-badge
   :target: https://gitlab.com/protis/docker/commits/main
   :alt: pipeline status

.. image:: https://img.shields.io/docker/v/benvial/protis?color=8678bd&label=dockerhub&logo=docker&logoColor=white&style=for-the-badge
  :target: https://hub.docker.com/repository/docker/benvial/protis
  :alt: dockerhub

.. image:: https://img.shields.io/docker/pulls/benvial/protis?color=8678bd&style=for-the-badge
  :alt: docker pulls

.. image:: https://img.shields.io/docker/image-size/benvial/protis?color=8678bd&style=for-the-badge
  :alt: docker image size
  
.. image:: https://img.shields.io/badge/license-GPLv3-blue?color=bb798f&style=for-the-badge
  :target: https://gitlab.com/protis/docker/-/blob/main/LICENCE.txt
  :alt: license



Docker for protis
=================

Container with the protis code.

